from __future__ import unicode_literals

from django.db import models


class Centro(models.Model):
	nombre = models.CharField(max_length=100)
	id_centro = models.IntegerField()
	def __str__(self):
		return self.nombre

class Vehiculo(models.Model):
	nombre = models.CharField(max_length=100)
	id_vehiculo = models.IntegerField()

	def __str__(self):
		return self.nombre

class Inspeccion(models.Model):
	fecha_inspeccion = models.DateField()
	Centro = models.ForeignKey(Centro , on_delete = models.CASCADE)
	Vehiculo = models.ForeignKey(Vehiculo , on_delete = models.CASCADE)