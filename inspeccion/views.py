from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth.views import redirect_to_login
from django.views import View
from inspeccion.models import Centro, Vehiculo
from inspeccion.forms import CentroForm, VehiculoForm, InspeccionForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum

# Create your views here.


class ListaCentros(View):
	template_name = 'inspeccion/lista_centros.html'

	def get(self, request, *args, **kwargs):
		centros = Centro.objects.all()
		return render(request, self.template_name, { 'centros' : centros })


class NuevoCentro(View):
	template_name = 'inspeccion/editar_centro.html'
	form_class = CentroForm

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspeccion:listacentros'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(NuevoCentro, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspeccion:nuevocentro'))





class EditarCentro(View):
	template_name = 'inspeccion/editar_centro.html'
	form_class = CentroForm

	def get(self, request, *args, **kwargs):
		c_id = kwargs['id']
		c = get_object_or_404(Centro, pk = c_id)
		if not c:
			return redirect(reverse('inspeccion:listacentros'))

		form = self.form_class(instance = c)
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		c_id = kwargs['id']
		c = get_object_or_404(Centro, pk = c_id)
		if not c:
			return redirect(reverse('inspeccion:listacentros'))
		form = self.form_class(request.POST, instance = c)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspeccion:listacentros'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(EditarCentro, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspeccion:editarcentro', kwargs = { 'id' : kwargs['id'] }))




class EliminarCentro(View):

	def get(self, request, *args, **kwargs):
		return redirect(reverse('inspeccion:listacentros'))

	def post(self, request, *args, **kwargs):
		c_id = request.POST.get('id')
		c = get_object_or_404(Centro, pk = c_id)
		c.delete()
		return redirect(reverse('inspeccion:listacentros'))

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(EliminarCentro, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspeccion:listacentros'))









def lista_vehiculos(request):
	template_name = 'inspeccion/lista_vehiculos.html'

	if request.method == 'GET':
		
		vehiculos = Vehiculo.objects.all()

		return render(request, template_name, { 'vehiculos' : vehiculos })




def nuevo_vehiculo(request):
	template_name = 'inspeccion/editar_vehiculo.html'
	form_class = VehiculoForm

	
	if request.method == 'POST':
		form = form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspeccion:listavehiculos'))
	else:
		form = form_class()
	return render(request, template_name, { 'form' : form })





def editar_vehiculo(request, id):
	template_name = 'inspeccion/editar_vehiculo.html'
	form_class = VehiculoForm

	if not request.user.is_staff:
		return redirect_to_login(reverse('inspeccion:editarvehiculo', kwargs = { 'id' : id }))

	vehiculo = get_object_or_404(Vehiculo, pk = id)
	if not vehiculo:
		return redirect(reverse('inspeccion:listavehiculos'))

	if request.method == 'POST':
		form = form_class(request.POST, instance = vehiculo)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspeccion:listavehiculos'))
	else:
		form = form_class(instance = vehiculo)
	return render(request, template_name, { 'form' : form })





def eliminar_vehiculo(request):
	idvehiculo = request.POST.get('id')
	vehiculo = get_object_or_404(Vehiculo, pk = idvehiculo)
	idcir = vehiculo.centro.id

	if not request.user.is_staff:
		return redirect_to_login(reverse('inspeccion:listavehiculos', kwargs = { 'idcir' : idcir }))

	if request.method == 'POST':
		vehiculo.delete()
		
	return redirect(reverse('inspeccion:listavehiculos', kwargs = { 'idcir' : idcir }))



class NuevaInspeccion(View):
	template_name = 'inspeccion/nueva_inspeccion.html'
	form_class = InspeccionForm

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspeccion:listacentros'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(NuevaInspeccion, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspeccion:nuevainspeccion'))





def index(request):
	template_name = 'inspeccion/index.html'
	
	return render(request, template_name)